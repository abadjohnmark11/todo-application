<?php


    function displayAll(){
        $con = new mysqli('localhost', "root", "", 'employee');
    
        if ($con->connect_error) {
            die("Connection failed: " . $con->connect_error);
        }
        $query = "select * from employee";
        $result = $con->query($query);
        if ($result->num_rows > 0) {
            
            echo "<div style='display: flex; flex-direction: column; justify-content: center; align-items: center;'>";
            echo "<a href=addemployee.php >Add Employee</a></br>";
            echo "<form>";
            echo    "<table border=1>";
            echo        "<thead>";
            echo            "<tr>";
            echo                "<th>Firstname</th>";
            echo                "<th>Lastname</th>";
            echo                "<th>Middlename</th>";
            echo                "<th>Birthday</th>";
            echo                "<th>Address</th>";
            echo            "</tr>"; 
            echo        "</thead>";
            while ($row = $result->fetch_assoc()) {
                echo "<tr>";
                echo    "<td>" . $row['first_name'] . "</td>";
                echo    "<td>" . $row['last_name'] . "</td>";
                echo    "<td>" . $row['middle_name'] . "</td>";
                echo    "<td>" . $row['birthday'] . "</td>";
                echo    "<td> " . $row['address'] . "</td>";
                echo    "<td><a href='updateemployee.php?updateId=".$row['id']."'>Update</a></td>";
                echo    "<td><a href='listallemployee.php?deleteId=".$row['id']."'>Delete</a></td>";
                echo "</tr>";
            }
            echo        "<tbody>";
    
            echo        "</tbody>";
            echo    "</table>";
            echo "</form>";
            echo "</div>";
        }

    }
    
    $hasRequest = FALSE;
    //ADD
    if (isset($_POST['submit'])) {
        $hasRequest = TRUE;
        $firstname = $_POST['firstname'];
        $lastname = $_POST['lastname'];
        $middlename = $_POST['middlename'];
        $birthday = $_POST['birthday'];
        $address = $_POST['address'];


        $con = new mysqli('localhost', "root", "", 'employee');

        $query = "insert into employee (first_name, last_name, middle_name, birthday, address)
                        values ('" . $firstname . "','" . $lastname . "','" . $middlename . "','" . $birthday . "','" . $address . "')";
        if ($con->query($query) === TRUE) {
            echo "Employee inserted";
            displayAll();
        } else {
            echo "Error: " . $query . "<br>" . $con->error;
        }
        $con->close();
    }
    
    //UPDATE
    if(isset($_POST['saveupdate'])){
        
        $hasRequest = TRUE;
        $staticId = $_POST['id'];
        $firstnameupdate = $_POST['firstname'];
        $lastnameupdate = $_POST['lastname'];
        $middlenameupdate = $_POST['middlename'];
        $birthdayupdate = $_POST['birthday'];
        $addressupdate = $_POST['address'];

        $con = new mysqli('localhost', "root", "", "employee");

        $query = "update employee 
                    set
                        first_name = '".$firstnameupdate."',
                        last_name = '".$lastnameupdate."',
                        middle_name = '".$middlenameupdate."',
                        birthday = '".$birthdayupdate."',
                        address = '".$addressupdate."'
                    where
                        id = ".$staticId."
                ";
        $result = $con->query($query);

        if($result === TRUE){
            echo "Updated successfully";
            displayAll();
        }
        else{
            echo "Error updating record: " . $con->error;
        }
        
        $con->close();
    }

    //DELETE
    if (isset($_GET['deleteId'])) {
        $hasRequest = TRUE;
        $id = $_GET['deleteId'];

        $con = new mysqli("localhost", "root", "","employee");
        $query = "delete from employee where id = ".$id."";
        if($con->query($query) === TRUE){
            echo "Deleted successfully";
            displayAll();
        }
        
        else{
            echo "Error deleting record: " . $con->error;
        }
    }

    //IF no requst happen
    if(!($hasRequest)){
        displayAll();
    }

?>  