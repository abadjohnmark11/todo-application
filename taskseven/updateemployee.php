<?php

if (isset($_GET['updateId'])) {
    $id = $_GET['updateId'];
    $firstname = "";
    $lastname = "";
    $middlename = "";
    $birthday = "";
    $address = "";

    $con = new mysqli('localhost', "root", "", "employee");

    if ($con->connect_error) {
        die("Connection failed: " . $con->connect_error);
    }

    $query = "select * from employee where id = " . $id . "";
    $result = $con->query($query);

    if ($result->num_rows > 0) {
        while ($row = $result->fetch_assoc()) {
            $firstname = $row['first_name'];
            $lastname = $row['last_name'];
            $middlename = $row['middle_name'];
            $birthday = $row['birthday'];
            $address = $row['address'];
        }
    } else {
        echo "No records found";
    }

}


?>

<div style="text-align:center;"><a href="listallemployee.php">Display All Employee</a></div></br>
<form method="POST" action="listallemployee.php" style="display: flex; flex-direction:column; justify-content: center; align-items: center;">
    <h1>Update Employee</h1></br>
    <label for="name">ID:</label>
    <input type="text" name="id" value="<?php echo $id ?>" readonly></br>
    <label for="name">Firstname:</label>
    <input type="text" name="firstname" value="<?php echo $firstname; ?>"></br>

    <label for="name">lastname:</label>
    <input type="text" name="lastname" value="<?php echo $lastname; ?> "></br>

    <label for="name">Middlename:</label>
    <input type="text" name="middlename" value="<?php echo $middlename; ?> "></br>

    <label for="name">Birthday:</label>
    <input type="text" name="birthday" value="<?php echo $birthday; ?> "></br>

    <label for="name">Address:</label>
    <input type="text" name="address" value="<?php echo $address; ?> "></br>
    <button type="submit" name="saveupdate">Submit</button>
</form>