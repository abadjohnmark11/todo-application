<?php
        #task six
        $con =new mysqli("localhost", "root", "", "employee");
        
        if($con->connect_errno){
            die("Database connection failed: ". $con->connect_error);
        }
        
        $query = "insert into employee (first_name, last_name, middle_name, birthday, address)
                                values ('Jane', 'Doe', 'Marie', '1990-01-01', '456 Elm Street')";
        if($con->query($query) === TRUE){
            echo "New record inserted";
        }
        else{
            echo "Error: ". $query. "<br>". $con->error;
        }

        $query =  "select first_name, last_name, middle_name, birthday, address from employee";
        $result = $con->query($query);

        if($result->num_rows > 0){
            while($row = $result->fetch_assoc()){
                echo "First Name: " . $row["first_name"]. " - Last Name: " . $row["last_name"]. " - Birthday: " . $row["birthday"]. "<br>";
            }
        }
        else{
            echo "No data from databse yet";
        }

        $query = "select first_name, last_name, middle_name, birthday, address from employee where last_name like 'D%' ";
        $result = $con->query($query);

        if($result-> num_rows > 0){
            while($row = $result->fetch_assoc()){

                echo "First Name: " . $row["first_name"]. " - Last Name: " . $row["last_name"]. " - Birthday: " . $row["birthday"]. "<br>";
            }
        }
        else{
            echo "No result";
        }

        $query ="select first_name, last_name, middle_name, birthday, address from employee where id = (select MAX( id ) from employee) ";
        $result = $con->query($query);

        if($result-> num_rows > 0){
            while($row = $result->fetch_assoc()){

                echo "First Name: " . $row["first_name"]. " - Last Name: " . $row["last_name"]. " - Birthday: " . $row["birthday"]. "<br>";
            }
        }
        else{
            echo "No result";
        }
        

        $query = "update employee set address = '123 Main Street' where first_name = 'John' ";
        $result = $con->query($query);

        if($result === TRUE){
            echo "Employee updated succesfully <br>";
        }
        else{
            echo "Error: ". $query. "<br>". $con->error;
        }

        $query = "delete from employee where last_name like 'D%'";
        $result = $con->query($query);

        if($result === TRUE){
            echo "Deleted successfully";
        }
        else{
            echo "Error: ". $query. "<br>".$con->error;
        }
?>