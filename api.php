<?php

class API {
    public function printName($name) {
        echo "Full Name: " . $name . "<br>";
    }

    public function printHobbies($hobbies) {
        echo "Hobbies: <br>";
        foreach ($hobbies as $hobby) {
            echo "&nbsp;&nbsp;&nbsp;".$hobby . "<br> ";
        }
        echo "<br>";
    }

    public function printDetails($details) {
        foreach($details as $keys => $value){
            if(strtolower($keys) === 'email'){
                echo ucfirst(($keys) . ": ". $value)."<br>";
            }else{
                echo ucfirst(($keys) . ": ". ucfirst($value))."<br>";
            }
        }
    }
}

$api = new API();

$api->printName("John Mark L. Abad");
$api->printHobbies(["Cycling", "Coding", "Basketball", "Listening Music"]);
$mydetails = (object) [
    'age' => 24,
    'email' => 'abadjohnmark11@gmail.com',
    'birthday' => 'december 11, 1998'
];
$api->printDetails($mydetails);

?>
